import math
import os
from PIL import Image,ImageFont, ImageDraw
fontColor = (255,255,255,255)
backgroundColor = (0,0,0,255)
resolution = (1920,1080)
fontName="firacode.ttf"
fontSize=16

def opTuple(tup1, tup2, operator):
    newTup = [] 
    for i,j in zip(tup1,tup2):
        newTup.append(operator(i,j))

    return tuple(newTup)
def mulTuple(tup1, tup2):
    return opTuple(tup1, tup2, lambda x,y: x*y)
def addTuple(tup1, tup2):
    return opTuple(tup1, tup2, lambda x,y: x+y)
def divTuple(tup1, tup2):
    return opTuple(tup1, tup2, lambda x,y: x//y)

class Typer:
    def __init__(self):
        self.resolution=resolution
        self.image = Image.new("RGB",self.resolution,color=backgroundColor) 
        self.font = ImageFont.truetype(font=fontName,size=fontSize)
        self.draw = ImageDraw.Draw(self.image)
        self.cursorPos = (0,0)
        self.textBounds = divTuple(self.resolution,self.font.getsize("A"))
        self.frame = 0
    
    def recreateImage(self):
        self.image = Image.new("RGB",self.resolution,color=backgroundColor)
        self.draw = ImageDraw.Draw(self.image)
        self.textBounds = divTuple(self.resolution,self.font.getsize("A"))


    def save(self,filename=None):
        if filename is None:
            self.image.save(str(self.frame) + ".png")
            #print(self.frame)
            self.frame += 1
        else:
            self.image.save(filename + ".png")

    def clearScreen(self):
        self.draw.rectangle([(0,0),self.resolution],fill=backgroundColor)
        self.cursorPos = (0,0)

    def checkCursorBounds(self):
        if self.cursorPos[0] >= self.textBounds[0]:
            self.cursorPos = addTuple(self.cursorPos,(0,1))
            self.cursorPos = mulTuple(self.cursorPos,(0,1))
        if self.cursorPos[1] >= self.textBounds[1]:
            self.clearScreen()

    def incCursor(self): #increment cursor one space to the right
        self.cursorPos = addTuple(self.cursorPos,(1,0))
        self.checkCursorBounds()

    def setCursorPos(self,newCursorPos):
        self.cursorPos = newCursorPos
        self.checkCursorBounds()

    def getCursorPos(self):
        return self.cursorPos

    def getPixelPos(self):
        return mulTuple(self.getCursorPos(),self.font.getsize('A'))


    def newLine(self):
        #move cursor to beginning of new line 
        self.cursorPos = addTuple(mulTuple(self.cursorPos,(0,1)),(0,1))
        self.checkCursorBounds()
    
    def typeChar(self,letter,save=True):
        self.drawCursor(erase=True,save=False)
        self.draw.text(self.getPixelPos(), letter, font=self.font,fill=fontColor)
        self.incCursor()
        self.drawCursor(save=False)
        if save == True:
            self.save()

    def typeString(self,string,save=True):
        for i in string:
            self.drawCursor(erase=True,save=False)
            self.draw.text(getPixelPos(),i,font=self.font,fill=fontColor)
            self.incCursor()
            self.drawCursor(save=False)
            if save == True:
                self.save()
        self.drawCursor(erase=True,save=False)

    def putChar(self,letter,position,save=True):
        self.draw.text(position,letter,font=self.font,fill=fontColor)
        if save == True:
            self.save()

    def putString(self,string,position,save=True):
        p = position
        for i in string:
            self.draw.text(p,i,font=self.font,fill=fontColor)
            p = addTuple(p,(self.font.getsize('A')[0],0))
            if save == True:
                self.save()

    def drawCursor(self,erase=False,save=True):
        startCorner = mulTuple(self.cursorPos,self.font.getsize("A"))
        endCorner = addTuple(startCorner,self.font.getsize("A"))
        if erase == True:
            self.draw.rectangle([startCorner,endCorner],fill=backgroundColor)
        else:
            self.draw.rectangle([startCorner,endCorner],fill=fontColor)
        if save == True:
            self.save()

    def progressBar(self,step=1,save=True):
        startChar = self.cursorPos[0]
        endChar = self.textBounds[0]-4 #Minus "100%"
        distance = endChar - startChar
        for i in range(0,100):
            cursorPos = (startChar + i//(100/distance),self.cursorPos[1])
            percentPos = addTuple(cursorPos,(1,0))
            pixelLoc = getPixelPos()
            #THIS IS A HACK!!!j
            prevPixelLoc = mulTuple(addTuple(cursorPos,(-1,0)),self.font.getsize('A'))
            self.draw.text(prevPixelLoc,'█', font=self.font,fill=fontColor)
            #HACK CONCLUDED
            endPixelLoc = addTuple(pixelLoc,self.font.getsize('A'))
            percentPos = mulTuple(percentPos,self.font.getsize('A'))
            endPercentPos = addTuple(percentPos,self.font.getsize('100%'))
            self.draw.rectangle([pixelLoc,endPercentPos],fill=backgroundColor)
            self.draw.text(pixelLoc,'█', font=self.font,fill=fontColor)
            self.draw.text(percentPos, str(i) + "%", font=self.font,fill=fontColor)
            if save == True and i % step == 0:
                self.save()
        #last update
        self.draw.rectangle([percentPos,endPercentPos],fill=backgroundColor)
        self.draw.text(percentPos,"100%",font=self.font,fill=fontColor)
        if save == True:
            self.save()

    def pause(self):
        self.drawCursor()
        self.drawCursor(erase=True)

    def convertImage(self,image,convertFunction,save=True):
        image.load()
        letter = 0
        #Resolution of image is dependent on input image size
        self.resolution = mulTuple(image.size,self.font.getsize('A'))
        self.recreateImage()
        for j in range(0,image.size[1]):
            for i in range(0,image.size[0]):
                #if comparePixel(image.getpixel((i, j)),(0,0,0)) >= comparePixel((10,10,10),(0,0,0)):
                self.putChar(convertFunction(image.getpixel((i,j))),mulTuple((i,j),self.font.getsize('A')),save=False)
        
        if save==True:
            self.save()

def comparePixel(pixel1,pixel2):
    summ = 0
    for i,j in zip(pixel1,pixel2):
        summ += pow((i-j),2)

    return math.sqrt(summ)
        

def arcSpiral(theta,rot):
    a,b = 0,10
    center = mulTuple(resolution,(0.5,0.5))
    zeroPos = ((a+b*theta)*math.sin(theta),(a+b*theta)*math.cos(theta))
    rotPos = (zeroPos[0]*math.cos(rot)+zeroPos[1]*math.sin(rot),-zeroPos[0]*math.sin(rot)+zeroPos[1]*math.cos(rot))
    return addTuple(rotPos,center)

def loading():
    typer.typeString("Login:")
    typer.pause()
    typer.typeString("viperzer0")
    typer.newLine()
    typer.typeString("Password:")
    typer.pause()
    typer.newLine()
    typer.typeString("Last login: Fri August 14 20:32:10 on tty1.")
    typer.newLine()
    typer.typeString("[viperzer0@machina ~]$ ")
    typer.pause()
    typer.typeString("cd memories")
    typer.newLine()
    typer.typeString("[viperzer0@machina memories]$ ")
    typer.pause()
    typer.typeString("rm -rf *")
    typer.newLine()
    typer.typeString("WARNING! The command you have entered will permanently erase all memories from subject viperzer0! This cannot be undone! Are you sure you want to continue? [Y/N]")
    typer.pause()
    typer.typeString("Y")
    typer.newLine()

    for i in range(0,1000):
        typer.placeString(f"Removing memory{i:0>3}.dat:",save=False)
        typer.progressBar(step=100)
        typer.newLine()

def spiral():
    for rot in range(0,int(20*math.pi),1):
        for i in range(0,int(250*math.pi),1):
            typer.putString("HA",arcSpiral(i/4.0,rot/10.0),save=False)
        
        typer.save()
        typer.clearScreen()

def animate():
    files = os.listdir("./input")
    files.sort(key=lambda x: int(x[:-4]))
    for f in files:
        im = Image.open("./input/" + f)
        typer.convertImage("KILLME.",im,save=False)
        typer.save()
        typer.clearScreen()

def convert(pixel):
    if comparePixel(pixel,(85,255,255,255)) < 1.0:
        return 'A'
    elif comparePixel(pixel,(255,85,255,255)) < 1.0:
        return 'H'
    elif comparePixel(pixel,(255,255,255,255)) < 1.0:
        return 'Z'
    else:
        return ' '

typer = Typer()
im = Image.open("./input/input.png")
typer.convertImage(im,convert,save=True)
